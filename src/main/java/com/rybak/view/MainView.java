package com.rybak.view;

import java.util.*;

public class MainView {

    static Scanner scanner = new Scanner(System.in);
    private Map<String, String> menu;
    private Locale locale;
    private ResourceBundle bundle;
    private ViewMethods viewMethods;

    public MainView() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MainView", locale);
        setMenu();
        viewMethods = new ViewMethods();
    }

    public void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));

    }

    private void button0() {
        System.out.println("Good luck");
        scanner.close();
        System.exit(0);
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.print("Please, select menu point: ");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                viewMethods.getMethodsMenu().get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        this.button0();
    }
}
