package com.rybak.view;


import com.rybak.controller.ControllerImpl;
import com.rybak.model.Word;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ViewMethods {

    private Map<String, Printable> methodsMenu;
    private ControllerImpl controller;

    public ViewMethods() {
        controller = new ControllerImpl();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::doTask1);
        methodsMenu.put("2", this::doTask2);
        methodsMenu.put("3", this::doTask3);
        methodsMenu.put("4", this::doTask4);
        methodsMenu.put("5", this::doTask5);
        methodsMenu.put("6", this::doTask6);
        methodsMenu.put("7", this::doTask7);
    }

    private void doTask1() {
        System.out.println(controller.doTask1());
    }

    private void doTask2() {
        System.out.println(controller.doTask2());
    }

    private void doTask3() {
    }

    private void doTask4() {
        System.out.println(
                "Please enter length of words you want to printout from interrogative sentences:");
        controller.doTask4(MainView.scanner.nextInt())
                .forEach(System.out::println);
    }

    private void doTask5() {
    }

    private void doTask6() {
        List<String> list = controller.doTask6();
        StringBuilder space = new StringBuilder("\t");
        for (char j = 'a'; j < 'z'; j++) {
            for (String sortedWord : list) {
                if (sortedWord.charAt(0) == j) {
                    System.out.println(space + sortedWord);
                }
            }
            space.append("\t");
        }
    }

    private void doTask7() {
        Map<Word, Double> map = controller.doTask7();
        for (Map.Entry<Word, Double> entry : map.entrySet()) {
            System.out.print("Word= " + entry.getKey().getWord() + ", Percentage= ");
            System.out.printf("%.2f", entry.getValue());
            System.out.print("%");
            System.out.println();
        }
    }

    public Map<String, Printable> getMethodsMenu() {
        return methodsMenu;
    }
}
