package com.rybak;

import com.rybak.view.MainView;

public class App {

    public static void main(String[] args) {
        new MainView().show();
    }
}
